﻿using UnityEngine;
using System.Collections;

public class BackgroundMovement : MonoBehaviour
{
    private float ySpeed = -0.005f;

    void Update ()
    {
        //If below camera view, i.e. bottom most background segment, move above other background segments (+42.9 in world coordinates)
        if (transform.position.y < Camera.main.transform.position.y - 14)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 42.9f);
        }
    }

    void FixedUpdate ()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + ySpeed);  //Move background segment downwards
    }
}
