﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour
{

    public Vector3 moveTo;
    private float yOffset = 3.5f;  //Y offset so camera is pushed up a bit from player's y pos
    public GameObject player;
    private float moveTime = 0.05f;
    public GameObject countdown;
    private bool playedCountdown;   //Played countdown yet?
    public bool follow = true;  //Follow player?

    void Start()
    {
        moveTo = transform.position;
    }

    void Update()
    {
        if (player != null && follow) moveTo = new Vector3(0, player.transform.position.y + yOffset, -10); //Has to be -10, or everything goes invisible
        transform.position = Vector3.Lerp(transform.position, moveTo, moveTime);

        //Show countdown when player spawned, near to player focus
        if (player != null && transform.position.y > moveTo.y - 1 && !playedCountdown)
        {
            GameObject c = Instantiate(countdown, new Vector3(transform.position.x, transform.position.y + 5), Quaternion.identity) as GameObject;
            c.GetComponent<Countdown>().player = player;

            playedCountdown = true;
        }
       
       
    }

}
