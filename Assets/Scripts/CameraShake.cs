﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{

    private float shakeDuration = 0.5f;
    private float shakeT;   //Timer
    private float shakeAmount = 0.075f;
    private float moveTime = 0.1f;  //How long to take to move when lerping

    void Start ()
    {
        shakeT = shakeDuration; //Set this so that camera will not shake when starting since shakeT is not less than shakeDuration
    }

    public void Shake ()
    {
        shakeT = 0; //Set to 0 so now shakeT is less than shakeDuration, therefore meaning that the camera will shake in Update ()
    }

    void Update ()
    {
        if (shakeT < shakeDuration)
        {
            shakeT += Time.deltaTime;
            transform.localPosition += Random.insideUnitSphere * shakeAmount;   //Move camera to random position
        }
    }
}
