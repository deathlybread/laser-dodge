﻿using UnityEngine;
using System.Collections;

public class Countdown : MonoBehaviour
{

    public Sprite[] sprites;    //Array holding different sprites for different countdown nos. - 0 = 3, 1 = 2, 2 = 1, 3 = "GO!"
    private SpriteRenderer sprComp;
    private int c = 0;    //Counter to use when referencing index of array
    private Vector3 startPos;   //Starting position - we want to move back here when countdown has gone below camera view
    private Vector3 moveTo; //Position for countdown timer to move to
    private float moveTime = 0.05f;
    private AudioSource[] audioComps;   //Audio components - countdown sound for numbers - audioComps[0], countdown sound for go - audioComps[1]
    private bool playedAudio = false;   //Player audio yet?
    public GameObject player;

    void Start ()
    {
        sprComp = GetComponent<SpriteRenderer>();
        audioComps = GetComponents<AudioSource>();

        startPos = transform.position;
        moveTo = new Vector3(0, Camera.main.transform.position.y - 7, 0);    //we have to have 0 as z coordinate since Camera.main.transform.position has z coordinate of -10
    }

    void FixedUpdate ()
    {
        transform.position = Vector3.Lerp(transform.position, moveTo, moveTime);

        //Play sound effect when at or below centre of screen
        if (transform.position.y <= Camera.main.transform.position.y && !playedAudio)
        {
            if (c == 3)
            {
                audioComps[1].Play();   //Play go sound when displaying "GO!"
                player.GetComponent<PlayerMovementKeyboard>().canMoveY = true;  //Start moving walls down
                player.GetComponent<PlayerMovementKeyboard>().respawning = false;
            }
            else audioComps[0].Play();  //Otherwise play number sound

            playedAudio = true;
        }
        
        //When countdown below camera view, teleport back to start position (above camera view) and decrement countdown number
        if (transform.position.y <= moveTo.y + 0.5 && c != 3)
        {
            transform.position = startPos;
            c++;    //Increment counter for sprite index
            sprComp.sprite = sprites[c];

            playedAudio = false;    //Set to false so will play with next countdown number
        }
        else if (transform.position.y <= moveTo.y + 0.5 && c == 3)//i.e. if countdown is "GO!" (end of countdown)
        {
            Destroy(this.gameObject);
        }
    }
}
