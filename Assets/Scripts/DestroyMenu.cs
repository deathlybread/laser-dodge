﻿using UnityEngine;
using System.Collections;

public class DestroyMenu : MonoBehaviour
{

    void Update ()
    {
        //Check if menu has gone out of view, i.e. below bottom of screen in world coordinates - if it has, destroy it
        Vector3 screen = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height)); //Get screen dimensions in world coordinates
        //Find distance between camera y pos (center of displayed screen) and bottom of the camera's view (camera y - screen height / 2) - If menu is below this, i.e. out of camera's view, destroy it
        if (transform.position.y < (Camera.main.transform.position.y - screen.y / 2) - 1) Destroy(this.gameObject); //-1 so menu gets destroyed a bit after it goes out of the camera's view, since it will only be destroyed when it is further out now so that we don't see it being destroyed
    }

}
