﻿using UnityEngine;
using System.Collections;

public class EnemyDestroy : MonoBehaviour
{

    private Animator animComp;
    private AudioSource audioComp;
    public GameObject[] destroyOthers;
    public bool destroyed = false; //Destroyed?

    void Start ()
    {
        animComp = GetComponent<Animator>();
        audioComp = GetComponent<AudioSource>();
    }

	public void DestroySelf (bool destroyOther) //destroyOther - do we want to destroy other enemies as a result of this enemy being destroyed? - Only do so to other "laser" components if this object is the first in said "laser" series to be destroyed
    {
        if (!destroyed)
        {
            GetComponent<BoxCollider2D>().enabled = false;  //Disable collider so we don't destroy player after they have destroyed this enemy

            animComp.SetTrigger("Destroyed");
            if (audioComp != null && audioComp.clip.name.Contains("Explosion")) audioComp.Play();  //Only play if it's the explosion sound, since other sounds may be attached - we use .Contains("Explosion") since some explosion sounds are Explosion2, 

            //Shake camera
            CameraShake shakeScript = Camera.main.GetComponent<CameraShake>();
            if (shakeScript != null) shakeScript.Shake();

            //Destroy other enemies linked to this enemy
            if (destroyOther && destroyOthers != null)
            {
                foreach (GameObject g in destroyOthers)
                {
                    //Destroy self, however don't destroy others since it will not be the first enemy to be destroyed in the series if this if statement is true
                    g.GetComponent<EnemyDestroy>().DestroySelf(false);
                }
            }
        }
    }

    void Update ()
    {
        if (!destroyed && animComp.GetCurrentAnimatorStateInfo(0).IsName("Explosion")) destroyed = true;
        else if (destroyed && !animComp.GetCurrentAnimatorStateInfo(0).IsName("Explosion")) Destroy(this.gameObject);   //Destroy when object has been "destroyed" and explosion (destroy animation) is complete
    }

}
