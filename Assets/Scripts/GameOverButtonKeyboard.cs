﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
using System.Collections;

public class GameOverButtonKeyboard : MonoBehaviour
{

    public enum Action { Retry = 0, ShowAd = 1 };
    public Action action;   //Which action does this button carry out
    public GameObject player;

    void FixedUpdate ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.up);    //Raycast from position where mouse is clicked (in world coordinates)

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == this.gameObject)
                {
                    if (action == Action.Retry) SceneManager.LoadScene("MenuGame"); //If retry, reload game scene
                    else ShowAd();
                }
            }
        }
    }

    private void ShowAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            player.GetComponent<PlayerMovementKeyboard>().Respawn();
            player.GetComponent<PlayerMovementKeyboard>().playedAd = true;
        }
    }

}
