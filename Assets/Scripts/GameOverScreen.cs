﻿using UnityEngine;
using System.Collections;

public class GameOverScreen : MonoBehaviour
{

    private Vector3 moveTo;
    private float moveTime = 0.05f;

    void Update ()
    {
        moveTo = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 10));  //In case camera is shaking, update to make sure game over screen goes to center of screen when camera is done shaking. We have to use "10" here since, for some reason, it goes out of view of camera otherwise
        transform.position = Vector3.Lerp(transform.position, moveTo, moveTime);
    }
}
