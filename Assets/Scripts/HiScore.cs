﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class HiScore : MonoBehaviour
{

    public Sprite[] digitsSpr = new Sprite[10]; //Array containing digit sprites
    public SpriteRenderer[] sprComp;    //Array of sprite renderers for each digit obj
    private int hiScore = 0;
    public LevelScore score;
    
    void Start ()
    {
        hiScore = PlayerPrefs.GetInt("HiScore");
    }
        
    void Update()
    {
        //Displaying score
        List<int> d = ConvertToDigits(hiScore);   //If current score less than or equal to hi score, display user's current hi score
        for (int x = 0; x < d.Count; x++)
        {
            sprComp[x].sprite = digitsSpr[d[x]];    //Set xth sprite component to sprite of xth digit
        }
    }

    private List<int> ConvertToDigits(int n)
    {
        List<int> digits = new List<int>(); //List to store each digit of int, n
        while (n > 0)
        {
            digits.Add(n % 10);
            n /= 10;
        }

        digits.Reverse();   //Reverse list so that 1st digit of number is at 0 index of list
        return digits;
    }

    public void SetHiScore ()
    {
        if (score.score > hiScore)
        {
            PlayerPrefs.SetInt("HiScore", score.score);
            PlayerPrefs.Save();
        }
    }

}
