﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class HowToPlay : MonoBehaviour
{

    public Sprite spr2;
    private SpriteRenderer sprComp;

    void Start ()
    {
        sprComp = GetComponent<SpriteRenderer>();
    }

    void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (sprComp.sprite != spr2) sprComp.sprite = spr2;
            else SceneManager.LoadScene("MenuGame");
        }
    }

}
