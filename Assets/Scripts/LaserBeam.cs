﻿using UnityEngine;
using System.Collections;

public class LaserBeam : MonoBehaviour
{

    Animator animComp;
    AudioSource audioComp;
    bool playedSound;

    void Start ()
    {
        animComp = GetComponent<Animator>();
        audioComp = GetComponent<AudioSource>();
    }

    void Update ()
    {
        if (animComp.GetCurrentAnimatorStateInfo(0).IsName("Activated") && !playedSound)
        {
            audioComp.Play();
            playedSound = true;
        }
    }
}
