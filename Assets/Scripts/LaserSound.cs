﻿using UnityEngine;
using System.Collections;

public class LaserSound : MonoBehaviour {

    private AudioSource audioComp;
    public AudioClip[] sounds;

    void Start ()
    {
        audioComp = GetComponent<AudioSource>();

        //Randomly pick which (explosion) sound to use
        int ran = Random.Range(0, sounds.Length);   //Pick random element of array, i.e. pick random sound
        audioComp.clip = sounds[ran];
    }
}
