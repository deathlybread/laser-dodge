﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour
{

    public GameObject wall;
    public GameObject player;
    public GameObject playerInstance;  //Instantiated player reference
    public CameraMove camMove;
    public GameObject ui;
    public GameObject pause;
    public LevelScore levelScore;    //Reference to LevelScore script on level obj (UI)
    public HiScore hiScore; //Reference to HiScore script on hi score obj (UI)

    public void GenerateStart(Vector3 pos, Vector3 playerPos)
    {
        ui.GetComponent<UiMove>().move = true;  //Move UI into position
        pause.GetComponent<PauseMove>().move = true;    //Move pause button into position

        //Instantiate player
        playerInstance = Instantiate(player, playerPos, Quaternion.identity) as GameObject;  //Instantiate player

        camMove.player = playerInstance;
        levelScore.player = playerInstance.GetComponent<PlayerMovementKeyboard>();

        playerInstance.GetComponent<PlayerCollision>().score = levelScore;
        playerInstance.GetComponent<PlayerCollision>().hi = hiScore;

        //Generate 3 other wall segments above starting wall segment
        GenerateWall(new Vector3(pos.x, pos.y));   //+6.3 to position wall segment just above starting wall segment
        GenerateWall(new Vector3(pos.x, pos.y + 6.66f));   //+12.6 to position wall segment just above wall above starting wall segment
        GenerateWall(new Vector3(pos.x, pos.y + 13.32f));  //+18.9 to position wall segment just above wall above wall above starting wall segment
    }

    private void GenerateWall (Vector3 pos)
    {
        GameObject w = Instantiate(wall, pos, Quaternion.identity) as GameObject;
        w.GetComponent<WallMovement>().player = playerInstance; //Set reference to player
    }

}
