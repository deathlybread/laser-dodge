﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelScore : MonoBehaviour
{

    public Sprite[] digitsSpr = new Sprite[10]; //Array containing digit sprites
    public int score = 0;
    public PlayerMovementKeyboard player;   //Reference to player movement script on player
    public SpriteRenderer[] sprComp;    //Array of sprite renderers for each digit obj

    void Update ()
    {
        //Displaying score
        List<int> d = ConvertToDigits(score);
        for (int x = 0; x < d.Count; x++)
        {
            sprComp[x].sprite = digitsSpr[d[x]];    //Set xth sprite component to sprite of xth digit
        }
    }
	
    private List<int> ConvertToDigits (int n)
    {
        List<int> digits = new List<int>(); //List to store each digit of int, n
        while (n > 0)
        {
            digits.Add(n % 10);
            n /= 10;
        }

        digits.Reverse();   //Reverse list so that 1st digit of number is at 0 index of list
        return digits;
    }
}
