﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuButton : MonoBehaviour
{

    public enum Action { Play = 0, HowToPlay = 1 };
    public Action action;   //This button's action to carry out when tapped
    private bool clicked = false;   //Clicked? - Only allow button to be clicked and carry out action once
    public LevelGenerator levGen;
    private AudioSource audioComp;

    void Start ()
    {
        audioComp = GetComponent<AudioSource>();
    }

    void Update ()
    {
        if (Input.GetMouseButtonDown(0) && !clicked)
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.up);    //Raycast from position where mouse is clicked (in world coordinates)

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == this.gameObject)
                {
                    switch (action)
                    {
                        case Action.Play:
                            StartGame();
                            break;
                        case Action.HowToPlay:
                            SceneManager.LoadScene("HowToPlay");
                            break;
                    }
                    clicked = true;

                    if (audioComp != null) audioComp.Play();    //Play sound effect
                }
            }
        }
    }

    private void StartGame()
    {
        float offsetY = 8.5f;    //Y offset from this object's pos to game start pos
        levGen.GenerateStart(new Vector3(0, transform.position.y + offsetY), new Vector3(0, transform.position.y + 5.75f));
    }

}
