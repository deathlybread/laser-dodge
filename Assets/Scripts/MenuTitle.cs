﻿using UnityEngine;
using System.Collections;

public class MenuTitle : MonoBehaviour
{

    public Vector3 moveTo;
    private float moveTime = 0.05f;
    private bool playedSound = false;

    void Update ()
    {
        if (!Application.isShowingSplashScreen)
        {
            if (transform.position != moveTo) transform.position = Vector3.Lerp(transform.position, moveTo, moveTime);    //Lerp to position
            if (GetComponent<AudioSource>() != null && GetComponent<AudioSource>().clip.name == "Title" && !playedSound)
            {
                GetComponent<AudioSource>().Play(); //Play sound effect
                playedSound = true;
            }
        }
    }
}
