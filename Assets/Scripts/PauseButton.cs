﻿using UnityEngine;
using System.Collections;

public class PauseButton : MonoBehaviour
{

    private bool paused;
    public GameObject snd;
    public GameObject rst;

    void Start ()
    {
        //Make sure game isn't paused when starting
        Time.timeScale = 1;
        paused = false;

        snd.SetActive(false);
        rst.SetActive(false);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.up);    //Raycast from position where mouse is clicked (in world coordinates)
            if (hit.collider != null)
            {
                if (hit.collider.gameObject == this.gameObject)
                {
                    if (!paused)
                    {
                        Time.timeScale = 0;
                        paused = true;

                        snd.SetActive(true);
                        rst.SetActive(true);
                    }
                    else
                    {
                        Time.timeScale = 1;
                        paused = false;

                        snd.SetActive(false);
                        rst.SetActive(false);
                    }
                }
            }
        }
    }
}
