﻿using UnityEngine;
using System.Collections;

public class PauseMove : MonoBehaviour
{

    public bool move = false;
    private Vector3 targetPos = new Vector3(-2.365f, -4.59f, 10);

	void Update ()
    {
        if (move && transform.localPosition != targetPos)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, 0.1f);
        }
    }
}
