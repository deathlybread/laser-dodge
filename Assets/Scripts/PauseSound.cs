﻿using UnityEngine;
using System.Collections;

public class PauseSound : MonoBehaviour
{

    public Sprite sndOn;
    public Sprite sndOff;
    private SpriteRenderer sprComp;

    void Start ()
    {
        sprComp = GetComponent<SpriteRenderer>();

        //Set sprite
        if (AudioListener.volume == 1) sprComp.sprite = sndOn;
        else sprComp.sprite = sndOff;

    }

    void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.up);    //Raycast from position where mouse is clicked (in world coordinates)

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == this.gameObject)
                {
                    if (AudioListener.volume == 1)
                    {
                        AudioListener.volume = 0;
                        sprComp.sprite = sndOff;
                    }

                    else
                    {
                        AudioListener.volume = 1;
                        sprComp.sprite = sndOn;
                    }
                }
            }
         }
    }
	
}
