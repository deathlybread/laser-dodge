﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour
{

    private Animator animComp;
    private AudioSource[] audioComps;   //[0] = die sound, [1] = coin sound
    public GameObject gameOverScreen;
    public GameObject gameOverScreenNoAd;
    public LevelScore score;
    public HiScore hi;
    public bool destroyed = false; //Player destroyed yet?
    public GameObject go;   //Game over screen instance

    void Start ()
    {
        animComp = GetComponent<Animator>();
        audioComps = GetComponents<AudioSource>();
    }

    void OnTriggerEnter2D (Collider2D coll)
    {
        if (coll.gameObject != null)
        {
            //Destroy laser if player collides with it (NOT when colliding with laser BEAM)
            if (coll.gameObject.tag == "Laser")
            {
                coll.gameObject.GetComponent<EnemyDestroy>().DestroySelf(true);
                Camera.main.GetComponent<CameraShake>().Shake();    //Shake camera
            }
            //Kill player if collides with laser BEAM or spike
            else if (coll.gameObject.tag == "LaserBeam" || coll.gameObject.tag == "Spike")
            {
                gameOver();
            }
            else if (coll.gameObject.tag == "Coin")
            {
                score.score++;
                Destroy(coll.gameObject);
                audioComps[1].Play();   //Play coin sound
            }
        }
    }

    private void gameOver ()
    {
        //Disable collider so we don't keep colliding w/ other enemies and keep getting more game over screens
        GetComponent<BoxCollider2D>().enabled = false;

        //Position to instantiate game over screen at
        Vector3 screenPos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height + 1, 10));

        //Show game over screen
        if (!GetComponent<PlayerMovementKeyboard>().playedAd) go = Instantiate(gameOverScreen, screenPos, Quaternion.identity) as GameObject;
        else go = Instantiate(gameOverScreenNoAd, screenPos, Quaternion.identity) as GameObject;


        foreach (GameOverButtonKeyboard g in go.GetComponentsInChildren<GameOverButtonKeyboard>())
        {
            g.player = this.gameObject;
        }

        animComp.SetTrigger("Destroyed");   //Play player explosion animation
        audioComps[0].Play();   //Play die sound
        Camera.main.GetComponent<CameraShake>().Shake();    //Shake camera
        transform.rotation = Quaternion.identity;   //Set rotation to 0, 0, 0

        hi.SetHiScore();    //Set new hi score if necessary

        destroyed = true;
    }

    void Update ()
    {
        //Check if explosion animation has finished
        if (destroyed && animComp.GetCurrentAnimatorStateInfo(0).IsName("Destroyed"))
        {
            GetComponent<PlayerMovementKeyboard>().canMoveY = false;
            this.gameObject.SetActive(false);
        }
    }

}
