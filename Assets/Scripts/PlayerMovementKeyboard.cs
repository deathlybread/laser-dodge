﻿using UnityEngine;
using System.Collections;

public class PlayerMovementKeyboard : MonoBehaviour
{

    private Rigidbody2D rigidbodyComp;
    private PlayerCollision collisionScript;
    public bool canMoveY;   //Can player move up? - i.e. can walls move down, since player doesn't move vertically - we use a reference to this public bool in each wall to determine if they can move down
    private Vector3 screenWorld; //Screen vector in world coordinates
    private Vector2 movement;
    private float angle;
    private float decel = 0.1f;  //Decleration rate
    public bool respawning = false;
    public GameObject countdown;
    public bool playedAd = false;

    void Start()
    {
        rigidbodyComp = GetComponent<Rigidbody2D>();
        collisionScript = GetComponent<PlayerCollision>();
        screenWorld = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
    }

    void Update()
    {
        //Get angle
        angle = transform.localEulerAngles.z;
        angle = (angle > 180) ? angle - 360 : angle;    //Convert angle to negative if necessary

        if (canMoveY && !collisionScript.destroyed)
        {
            //Input movement (KEYBOARD)
            if (Input.GetKey(KeyCode.LeftArrow) && angle < 45) transform.Rotate(0, 0, Time.deltaTime * 500);
            else if (Input.GetKey(KeyCode.RightArrow) && angle > -45) transform.Rotate(0, 0, Time.deltaTime * -500);

            //Input movement (TOUCH)
            foreach (Touch t in Input.touches)
            {
                if (t.position.x < Screen.width / 2 && angle < 45) transform.Rotate(0, 0, Time.deltaTime * 500);
                else if (t.position.x > Screen.width / 2 && angle > -45) transform.Rotate(0, 0, Time.deltaTime * -500);
            }
        }
    }

    void FixedUpdate ()
    {
        movement.x = -(angle / 7.5f); //Move in direction of angle (multiply by -1 to move in correct direction, otherwise direction reversed)
        //Apply movement
        if (!collisionScript.destroyed) rigidbodyComp.velocity = movement;
        else rigidbodyComp.velocity = Vector2.zero;

        //Check if player has gone off screen
        if (transform.position.x >= Camera.main.transform.position.x + screenWorld.x) transform.position = new Vector3(Camera.main.transform.position.x - screenWorld.x, transform.position.y); //Gone off right - reset at left
        else if (transform.position.x <= Camera.main.transform.position.x - screenWorld.x) transform.position = new Vector3(Camera.main.transform.position.x + screenWorld.x, transform.position.y);    //Gone off left - reset at right
    }

    public void Respawn ()
    {
        this.gameObject.SetActive(true);
        respawning = true;
        canMoveY = false;
        transform.position = new Vector3(0, transform.position.y);
        GetComponent<PlayerCollision>().destroyed = false;
        GetComponent<BoxCollider2D>().enabled = true;
        GetComponent<Animator>().SetTrigger("Respawn");
        Destroy(GetComponent<PlayerCollision>().go);

        GameObject c = Instantiate(countdown, new Vector3(0, Camera.main.transform.position.y + 5), Quaternion.identity) as GameObject;
        c.GetComponent<Countdown>().player = this.gameObject;
    }

}
