﻿using UnityEngine;
using System.Collections;

public class PlayerSound : MonoBehaviour
{

    private AudioSource[] audioComps;   //Array of audio source components, since there are multiple attached to player obj - we want audioComp[0] for the move sound
    private AudioSource audioMove;  //Move sound audio source
    private PlayerMovementKeyboard movementScript;
    private float waitDuration = 0.1f;  //Time between playing sound (in seconds)
    private float soundT = 0.0f;    //Elapsed time (in seconds)

    void Start ()
    {
        audioComps = GetComponents<AudioSource>();
        audioMove = audioComps[0];  //Get audio source with the move sound
        movementScript = GetComponent<PlayerMovementKeyboard>();
    }

    void Update ()
    {
        if (movementScript.canMoveY)
        {
            soundT += Time.deltaTime;   //Count elapsed time (in seconds)

            if (soundT >= waitDuration)
            {
                audioMove.Play();   //Play sound
                soundT = 0; //Reset timer
            }
        }
    }
	
}
