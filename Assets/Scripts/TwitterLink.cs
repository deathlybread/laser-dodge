﻿using UnityEngine;
using System.Collections;

public class TwitterLink : MonoBehaviour
{

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.up);    //Raycast from position where mouse is clicked (in world coordinates)

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == this.gameObject)
                {
                    Application.OpenURL("https://twitter.com/pmakesgames");
                }
            }
        }
    }
}
