﻿using UnityEngine;
using System.Collections;

public class UiMove : MonoBehaviour {

	private Vector3 targetPos = new Vector3(-0.665f, 4.92f, 10);  //Position where UI should be (where move to at start)
    public bool move = false;   //Move into position yet?  

    void Update ()
    {
        if (move && transform.localPosition != targetPos)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, 0.1f);
        }
    }
}
