﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WallGenerate : MonoBehaviour
{

    public GameObject[] enemiesLeft;
    public GameObject[] enemiesRight;
    public GameObject[] enemiesCenter;
    public GameObject coin;
    private float enemyOffsetX = 1.25f; //X offsets of spawned enemies from wall (element indexes correspond to enemy array indexes)
    private List<GameObject> generatedEnemies = new List<GameObject>();   //List of enemies this wall segment has generated
    private List<GameObject> coins = new List<GameObject>();

	public void Generate ()
    {
        //Destroy already spawned enemies and coins
        DestroyEnemies();
        DestroyCoins();

        int side = Random.Range(0, 4);  //Randomly generate side - 0 = none (don't generate any enemies), 1 = left, 2 = right

        if (side != 0)
        {
            if (side == 1) //Left side
            {
                int i = Random.Range(0, enemiesLeft.Length); //Random index of array - "0" since we want left-side enemies, then randomly decide which enemy between 0 and 1
                GameObject e = enemiesLeft[i];  

                SpawnEnemy(e, new Vector3(transform.position.x - enemyOffsetX, transform.position.y));
                SpawnCoin(1);
            }
            else if (side == 2) //Right side
            {
                int i = Random.Range(0, enemiesRight.Length); //Random index of array - "0" since we want left-side enemies, then randomly decide which enemy between 0 and 1
                GameObject e = enemiesRight[i];  //"1" since we want right-side enemies, then randomly decide which enemy between 0 and 1

                SpawnEnemy(e, new Vector3(transform.position.x + enemyOffsetX, transform.position.y));
                SpawnCoin(3);
            }
            else if (side == 3)
            {
                int i = Random.Range(0, enemiesCenter.Length); //Random index of array - "0" since we want left-side enemies, then randomly decide which enemy between 0 and 1
                GameObject e = enemiesCenter[i];  //"1" since we want right-side enemies, then randomly decide which enemy between 0 and 1

                SpawnEnemy(e, new Vector3(0, transform.position.y));
            }
        }
    }

    private void SpawnEnemy (GameObject e, Vector3 pos)
    {
        GameObject s = Instantiate(e, pos, e.transform.rotation) as GameObject;
        s.transform.parent = transform; //Set parent of enemy to this wall segment
        generatedEnemies.Add(s);
    }

    private void SpawnCoin (int cPos)
    {
        GameObject c;
        switch (cPos)
        {
            case 1:
                c = Instantiate(coin, new Vector3(transform.position.x - 1, transform.position.y + 1), Quaternion.identity) as GameObject;
                c.transform.parent = transform;

                coins.Add(c);
                break;
            case 2:
                c = Instantiate(coin, new Vector3(transform.position.x, transform.position.y + 1), Quaternion.identity) as GameObject;
                c.transform.parent = transform;

                coins.Add(c);
                break;
            case 3:
                c = Instantiate(coin, new Vector3(transform.position.x + 1, transform.position.y + 1), Quaternion.identity) as GameObject;
                c.transform.parent = transform;

                coins.Add(c);
                break;
        }
    }

    private void DestroyEnemies ()
    {
        foreach (GameObject g in generatedEnemies)
        {
            Destroy(g);
        }
        generatedEnemies.Clear();
    }

    private void DestroyCoins ()
    {
        foreach (GameObject g in coins)
        {
            Destroy(g);
        }
        coins.Clear();
    }

    void Update ()
    {
        if (transform.parent.gameObject.GetComponent<WallMovement>().player.GetComponent<PlayerMovementKeyboard>().respawning)
        {
            DestroyEnemies();
            DestroyCoins();
        }
    }
}
