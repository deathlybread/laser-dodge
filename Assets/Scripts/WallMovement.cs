﻿using UnityEngine;
using System.Collections;

public class WallMovement : MonoBehaviour {

    public GameObject player;
    private float speedY = -0.1f;
    private float incrR = 2.5f; //Seconds after which to increase speedY by -0.001
    private float incrT = 0.0f;

    void FixedUpdate ()
    {
        if (player != null)
        {
            if (player.GetComponent<PlayerMovementKeyboard>().canMoveY) transform.position = new Vector3(0, transform.position.y + speedY);
        }

        //If below camera view, i.e. bottom most wall segment, move above other wall segments (+19.98 in world coordinates)
        if (transform.position.y < Camera.main.transform.position.y - 12)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 19.98f);

            //Generate new enemies in all wall segments
            foreach(WallGenerate w in GetComponentsInChildren<WallGenerate>())
            {
                w.Generate();
            }
        }
    }

    void Update()
    {
        //Increment speedY
        if (speedY > -0.15f)  //Max speedY = 0.1f - don't go lower than this
        {
            incrT += Time.deltaTime;
            if (incrT >= incrR)
            {
                speedY += -0.001f;
                incrT = 0;
            }
        }
    }

}
